---
title: 如何連到遠端伺服器
---
這是列出所有和遠端403電腦連接的指令
<!-- more -->
``` bash
$ ssh gislabdltb@140.120.26.158 -p 40322
```
密碼：Nchu52024101

root權限
``` bash
$ su
```
密碼：Nchu52024101

查看所有的images
``` bash
docker images
```

是指讓該image在背景執行，我是設定在gpu_final
``` bash
docker run -dit nvidia/cuda:tf_gpu_final
```

查看在背景執行中的image有哪些，
``` bash
docker ps
```

進入該執行中的image
``` bash
docker attach <id>
```

若要跳出該image但要讓他繼續執行：
先ctrl +p 再按q

然後做commit的動作，儲存該image所做的任何更動
``` bash
docker commit <id> <resoitory>:<tag names>
```
就會儲存成新的版本

可以在用docker images確認是否有這個image出現了。

在nvidia/cuda:tf_gpu_final 底下，在/usr/gislab_tensorflow/obj_detection/obj_detection做物件偵測吧。
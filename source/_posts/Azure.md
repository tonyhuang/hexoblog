---
title: Azure API 說明
---

這是列出人臉辨識關於Azure API的說明
<!-- more -->


#### API reference 的網站：
https://westus.dev.cognitive.microsoft.com/docs/services/563879b61984550e40cbbe8d/operations/563879b61984550f30395236


#### 上傳人臉到一個清單中：

方法：post

url : https://westcentralus.api.cognitive.microsoft.com/face/v1.0/facelists/tonylist/persistedFaces

以上是會傳到我建立的臉部清單tonylist中。

headers:
  "Ocp-Apim-Subscription-Key":"95500a0e2dfb40b2a53768210da58567",
  "Content-Type":"application/json"

以上的headers請根據實際Azure給的key去做使用。

raw ：

{
	"url":"https://firebasestorage.googleapis.com/v0/b/facestone-85b05.appspot.com/o/image%2Fface.jpg?alt=media&token=fb2433cf-a937-4397-92f3-c2eca05f361b"
}

這是firebase的url位置。

結果：
{
    "persistedFaceId": "396f353f-dc91-4b91-9f01-17624549291a"
}



#### detect : 判斷有沒有臉：

方法：post

url:
https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect

headers:

同上

raw data: 


{
           "url": "https://firebasestorage.googleapis.com/v0/b/facestone-85b05.appspot.com/o/image%2Fimage.jpg?alt=media&token=616ee767-7bf4-4680-9daf-04ff93c98aa6"
}

結果就是有沒有臉：
有得話就會回傳臉的位置：

[
    {
        "faceId": "61fd7731-c793-4292-be8c-6c4ec3a6740d",
        "faceRectangle": {
            "top": 12,
            "left": 0,
            "width": 102,
            "height": 90
        }
    }
]



#### 創建一個facelist的清單：

方法：put

url: https://westcentralus.api.cognitive.microsoft.com/face/v1.0/facelists/tonylist

raw data:

{
    "name":"tonylist",
    "userData":"tony face list"
}

headers：

同上


如果成功的話，就會有Status: 200 OK，因為是開空的清單，就不會有值回傳。

#### 查詢facelist的清單：

方法：get

url: https://westcentralus.api.cognitive.microsoft.com/face/v1.0/facelists/tonylist


headers：

同上


如果成功的話，就會列出當前tonylist的清單。


#### 找有沒有相似的臉，跟facelist去比對：

方法：post

url : https://westcentralus.api.cognitive.microsoft.com/face/v1.0/findsimilars

raw data:

{    
    "faceId":"5e236374-221c-4334-9fae-0c42c0aa7bc2",
    "faceListId":"tonylist",  
    "maxNumOfCandidatesReturned":1,
    "mode": "matchFace"
}

結果：
會吐出facelist中相似的id，以及信心值。
[
    {
        "persistedFaceId": "3af29533-72a8-430d-bd2b-bf8c8be76c26",
        "confidence": 1
    }
]
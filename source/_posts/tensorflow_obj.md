---
title: TensorFlow for object detection 2017/2/20
---



前言：

本文係教學如何使用tensorflow的物件偵測，包含自己訓練出模型到最後和自己所訓練的模型比對。

我所使用的版本是python3，所以記得在任何執行.py檔案時，都要以python3做執行。

<!-- more -->

### 啟動tensorflow 的virtualenv環境

然後，要先了解怎麼打開tensorflow的環境。我是使用virtualenv的方式安裝tensorflow，簡單好用，先決定要啟動哪一種版本的環境，

啟動CPU版本環境：
``` bash
$ source ~/tensorflow/bin/activate
```
成功的話:
{% asset_img tfcpu.png tfcpu %}

啟動GPU版本環境：
``` bash
$ source ~/tensorflow_gpu/bin/activate
```
成功的話:
{% asset_img tfgpu.png tfgpu %}

若成功的話，可以看到終端機前面的地方，會有出現環境名稱(tensorflow)或(tensorflow_gpu)，當然這個名稱不是死的，只是我當初建立此虛擬環境時就是這麼命名，若要查看詳細怎麼設定virtualenv，請查看tensorflow install 中的 [Installing with Virtualenv](https://www.tensorflow.org/install/install_linux)

不過打開了這個環境之後，有些套件如果是在自己的電腦底層，可能會找不到相關套件，像是本篇文章後續會提到的Qt的編譯安裝，就用不到tensorflow，此時就可以關閉tensorflow，用以下指令：

``` bash
$ deactivate
```

至此，就可以開始使用tensorflow相關的套件囉！


### 專案路徑說明

本篇文章所使用的tensorflow API，都是使用官方的github專案，當然我都已經先下載過，如有要自行下載的話，[點我看連結](https://github.com/tensorflow/models.git)
或是直接:

``` bash
$ git clone https://github.com/tensorflow/models.git
```

在403教室中的遠端伺服器電腦，我則是放在這個路徑（這裡我只安裝gpu版本的tensorflow喔）：

{% asset_img remote_project_path.png tfgpu %}


或是看這裡：
``` bash
 /usr/gislab_tensorflow/obj_detection/obj_detection
```
可以看到這個資料夾底下，有models這個資料夾，也就是我git clone後的位置。

# 然後我們另外自定義開了一個叫做/obj_detection的資料夾，是為了方便整理訓練前資料用的。請特別注意喔！

### [part.2 教你如何即時使用tensorflow 以前置鏡頭偵測物件(點我看影片)](https://www.youtube.com/watch?v=MyAOtvwTkT0)

*查看本文所有教學影片時，請記得若有提到底下的設定model/object_detection，在官方的資料夾已經改成model/research/object_detection，請特別注意。

###然後一定要記得設定環境變數，在models/research底下設定:
``` bash
 export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
```
再往下繼續設定。

{% asset_img videofeed.png Video feed image %}
<可參考part.2標題的連結影片>
目前程式碼已經寫好，只要在model/research/object_detection資料夾中下終端機指令，即可實現由前置鏡頭偵測物件：
``` bash
python3 object_detection_tutorial_CONVERTED.py
```
由於執行程式時需要些時間，似乎需要讀取tensorflow的相關檔案，看到前置鏡頭燈亮後，請稍待1~3分鐘，螢幕上就會顯示imshow所吐出的畫面。

執行成功的話，我是會吐出像這樣的訊息：

{% asset_img videofeed_terminal.png Video feed terminal %}



### [part.3 自定義想要被偵測的物件 - 標註影像資料產生xml檔案前期準備(點我看影片)](https://www.youtube.com/watch?v=K_mFnvzyLvc&t=1s)


本節係需要使用自定義的影像，所以要先準備需要被訓練的影像資料，我們必須要先對我們自己的影像手動上tag，再送入tensorflow做訓練，本節使用github上的https://github.com/tzutalin/labelImg ，將影像資料標註，我使用的是python3 + Qt5。

``` bash
$ git clone https://github.com/tzutalin/labelImg
```

然後以下指令：
``` bash
sudo apt-get install pyqt5-dev-tools
sudo pip3 install lxml
make qt5py3
python3 labelImg.py
```

我在實際開發時，都習慣用virtualenv開著tensorflow的環境，特別需要注意的是，安裝完後，因為make qt5py3 的步驟，需要關掉tensorflow環境才能編譯，反正這裡還用不到tensorflow環境，記得先關著。

最後，安裝完成以python3 labelImg.py執行時，可以看到跑出Qt的圖形介面：

{% asset_img labelimg1.png Labelimg Qt interface %}

選擇左方的Open Dir，然後選擇你預先放好你想要訓練的影像資料夾。
{% asset_img labelimg2.png choose dir %}

然後，根據你想訓練的物件，以左方的Create RectBox選取，在打上你要標註的類別名稱，然後左方的Save即可。好了的話，就按Next image，繼續標注下一張照片。
{% asset_img labelimg3.png rect box %}
記得，因為所有標註的照片中，理論上應該都要是相同的物件，所以記得標註名稱第一次輸入後，其他的影像就選擇一樣的即可囉！標註完之後，每張影像都會產生對應的xml檔案。


### [part.4 xml轉CSV檔，並產生TFrecord檔案(點我看影片)](https://www.youtube.com/watch?v=kq2Gjv_pPe8)

本節的目的，就是為了將xml轉成tfrecord的檔案。

上面part.3已經產生了xml檔案，根據影片作者敘述，接下來需要在你放影像的資料夾/images中，開兩個空個資料夾，分別是：test 和 train。
所以資料夾會長這樣：
{% asset_img imagesdir.png imagesdir %}

然後，將你剛剛產製的影像與xml檔案，選擇10%的檔案(影像與對應xml)，放入test，選擇其他的90%檔案(影像與對應xml)，放入train中。(這範例圖我只放一張而已，所以只有一張圖片跟一個對應xml檔案，理論上images資料夾裡面要有所有的影像資料)

接下來，我們就要把xml轉成csv檔案了，用以下連結：
https://github.com/datitran/raccoon_dataset
然後需要複製其中的xml_to_csv.py的檔案。然後需要按照影片改code。
(不過因為我已經寫好了，就用我的吧)

並且把xml_to_csv.py存在放這三個資料夾的目錄下，可以看上圖。

然後執行：
``` bash
$ python3 xml_to_csv.py
```

如果有出現像我的error：
ModuleNotFoundError: No module named 'pandas'
是因為我把這個套件用pip install pandas放在我tensorflow的virtualenv底下，所以，打開tensorflow的虛擬環境就好囉！

然後，回到raccoon的github專案，在github上以raw複製generate_tfrecord.py的程式碼，並在/obj_detection資料夾下存成同樣的檔名。然後，把標註的名稱改成自定義的名稱。找到程式碼第31行：

    if row_label == '標註名稱':

影片中作者是要辨識通心麵，所以他打macncheese，而我做測試時則是用frenchdog。把上面的標註名稱位置換掉即可。

**然後影片作者不知為啥在這裡先對tensorflow的models/research資料夾中先輸入了 
``` bash
sudo python3 setup.py install
```
應該是有什麼相依套件沒有安裝到，所以在這裡跳痛了一下。

然後回歸正題，理論上應該所有的設定都好了。接下來在自定義的/obj_detection資料夾下終端機輸入以下指令執行

generate_tfrecord.py：
``` bash
python3 generate_tfrecord.py --csv_input=data/train_labels.csv  --output_path=data/train.record
```
成功的話會顯示：
Successfully created the TFRecords: /Users/mac/HZC/test_of_tensorflow_obj_detecttion/obj_detection/data/train.record

``` bash
python3 generate_tfrecord.py --csv_input=data/test_labels.csv  --output_path=data/test.record
```
成功的話會顯示：
Successfully created the TFRecords: /Users/mac/HZC/test_of_tensorflow_obj_detecttion/obj_detection/data/test.record

好了，這時候可以到放csv檔的/data資料夾裡面看看有沒有產生test.record跟train.record檔案囉！
如下圖：
{% asset_img objdatalist.png objdatalist %}

那，part.4就到這裡囉！

### [part.5 準備訓練囉！(點我看影片)](https://www.youtube.com/watch?v=JR8CmWyh2E8)

本部分則是要開始準備訓練囉，在訓練前要準備一些設定，所以以下根據作者的步驟。

第一，先下載本專案到obj_detection(影片中的連結有誤，要改成這個)：
``` bash
wget http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v1_coco_11_06_2017.tar.gz
```

第二，複製以下的github的raw程式碼，但要改code。
https://github.com/derekjchow/models/blob/master/research/object_detection/samples/configs/ssd_mobilenet_v1_pets.config

在ssd_mobilenet_v1_pets.config中需要改的部分：
1. 把num_classes: 37 改成 num_classes: 1 
2. 把fine_tune_checkpoint: "PATH_TO_BE_CONFIGURED/model.ckpt"改成fine_tune_checkpoint: "ssd_mobilenet_v1_coco_11_06_2017/model.ckpt"
3. 有兩個同樣名稱的地方，把input_path: "PATH_TO_BE_CONFIGURED/pet_train.record"改成input_path: "data/train.record"
4. 有兩個同樣名稱的地方，把label_map_path: "PATH_TO_BE_CONFIGURED/pet_label_map.pbtxt"改成label_map_path: "data/object-detection.pbtxt"

好，到這裡應該是改完code了。

接下來，請到obj_detection/training資料夾裡面，產生一個文件，叫做：object-detection.pbtxt，裡面內容打入：
item {
    id: 1
    name: '標註名稱'
}

我的標註名稱是frenchdog，所以就填入後儲存在/training和/data資料夾。

接下來就要做很神奇的事情了：
請複製你自定義的資料夾obj_detection/中的幾個項目：
1. /data
2. /images
3. /ssd_mobilenet_v1_coco_11_06_2017
4. ssd_mobilenet_v1_pets.config
5. /training
如同作者影片中的截圖：
{% asset_img copying.png copy files %}

特別注意：ssd_mobilenet_v1_pets.config記得也放到/training中。
特別注意：object-detection.pbtxt也記得放到data裡面一份

複製到tensorflow官方的models/research/object_detection中，可以merge資料夾的話就merge吧，不要動到原本的檔案。除非原本的檔案中是你亂放的檔案那另當別論XD

接下來，請在model/research/object_detection資料夾下輸入終端機指令：

``` bash
$ python3 train.py --logtostderr --train_dir=training/ --pipeline_config_path=training/ssd_mobilenet_v1_pets.config
```

沒錯誤的話，應該就開始訓練囉！理論上他的loss值會收斂到某一個值，記得預設是0.5，如果沒有收斂到0.5的話，那代表就會一直跑訓練不會停，只有按ctrl+C才會停喔。如下圖所示：

{% asset_img trainingp.png trainingp %}




### [part.6 來看看訓練成果的偵測吧(點我看影片)](https://www.youtube.com/watch?v=srPndLNMMpk&t=635s)

在終端機輸入這個：

``` bash
$ python3 export_inference_graph.py \
    --input_type image_tensor \
    --pipeline_config_path training/ssd_mobilenet_v1_pets.config \
    --trained_checkpoint_prefix training/model.ckpt-200000 \
    --output_directory bana_graph
```
其中需要特別注意的：
--trained_checkpoint_prefix training/model.ckpt-XXXXX 中，XXXXX就看你訓練到多少steps，從models/research/object_detection/training去查看看所儲存的號碼有到多少。如下圖所示：
{% asset_img tm.png tm %}
所以依照這個例子，一定要確認有這三種檔案，然後我就應該改成model.ckpt-285囉。

--output_directory XXXXXXX_graph 可以自由取名，這會把整個model包起來成你XXXXXXX_graph的資料夾。


然後上述改完後，以jupyter notebook執行：

``` bash
jupyter notebook
```

開啟object_detection_tutorial.ipynb這個檔案。

然後對variable改成如圖：
{% asset_img vari.png variables part %}

當然，MODEL_NAME = '你前面產生graph資料夾的名稱，XXXXXXX_graph'
PATH_TO_LABELS = os.path.join('training', 'object-detection.pbtxt')
NUM_CLASSES = 1 (<這個是因為目前只有一種類別)


然後對Download Model刪除。
{% asset_img dm.png delete download model %}

最後，在models/research/object_detection/test_image底下，看你的image<編號>.jpg的編號有幾張是想被偵測的影像，你可以丟剛剛丟入訓練的影像。所以例如我有image1.jpg、image2.jpg、image3.jpg

就在Detection的部分，將range改成range(1,4)，以此類推囉。

然後按最上面的cell中的run all即可。
需要等待一下，拉到最下面，就可以看到標示的成果了。






# 如果之後只是要使用訓練的話，就不用上面所有的設定步驟都要做，所以，上面重要的步驟中，大概需要做：
1. 準備自己的images檔案
2. 使用labelimg 標示影像，產生xml
3. 透過xml_to_csv.py，產生csv
4. 透過generate_tfrecord.py，產生record檔案
5. 重複part.5 複製資料夾的動作，到models/research/object_detection裡面
6. 然後執行train.py的指令，要帶參數
7. part.6會產生XXXX_graph的資料夾(名稱是自己取的)，這整包就是模型了，要跑模型就複製這包東西，重複part.6的jupyter notebook設定即可。